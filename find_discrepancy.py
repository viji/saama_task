import requests
import json
from datetime import date, timedelta
from dateutil import parser
import sys
study_id="StudyHack"
domain_id="ae"
#getting AE subject id's
ae_url1 = "https://pyhack-dot-pharmanlp-177020.uc.r.appspot.com/api/1"+"/"+study_id+"/"+domain_id+"/subject"+"/list"

headers = {
    'Content-Type': "b00afc6d-66d2-4d0e-9e6e-ae1fc8cace58",
    'cache-control': "no-cache"
    }
try:
    ae_response_data1 = requests.get(ae_url1,headers=headers)
    ae_response_value1 = json.loads(ae_response_data1.text)
except:
    sys.exit("Unable to get the values!")
ae_subject_id=ae_response_value1['data'][:10]
ae_response_values=[]
#getting AE data using AE subject id's
for sub_id in ae_subject_id:
    ae_url2 = "https://pyhack-dot-pharmanlp-177020.uc.r.appspot.com/api/1"+"/"+study_id+"/"+domain_id+"/subject/"+str(sub_id)+"/list"
    try:
        ae_response_data2 = requests.get(ae_url2,headers=headers)
        ae_response_value2 = json.loads(ae_response_data2.text)
        ae_response_values.append(ae_response_value2)
    except:
        sys.exit("Unable to get the values!")
#getting CM subject id's
cm_url1 = "https://pyhack-dot-pharmanlp-177020.uc.r.appspot.com/api/1"+"/"+study_id+"/"+"cm"+"/subject"+"/list"

headers = {
    'Content-Type': "b00afc6d-66d2-4d0e-9e6e-ae1fc8cace58",
    'cache-control': "no-cache"
    }
try:
    cm_response_data1 = requests.get(cm_url1,headers=headers)
    cm_response_value1 = json.loads(cm_response_data1.text)
except:
    sys.exit("Unable to get the values!")

cm_subject_id=ae_response_value1['data'][:10]
cm_response_values=[]
#getting CM data using CM subject id's
for sub_id in ae_subject_id:
    cm_url2 = "https://pyhack-dot-pharmanlp-177020.uc.r.appspot.com/api/1"+"/"+study_id+"/"+"cm"+"/subject/"+str(cm_subject_id)+"/list"
    try:
        cm_response_data2 = requests.get(cm_url2,headers=headers)
        cm_response_value2 = json.loads(cm_response_data2.text)
        cm_response_values.append(cm_response_value2)
    except:
        sys.exit("Unable to get the values!")
type1ae=[]
type1cm=[]
ae=ae_response_values
cm=cm_response_values
for d in range(len(ae)):
    
    d1=ae[d]['data'] #data of AE
    d2=cm[d]['data'] #data of AE
    aeterm_values=[]
    for i in range(len(ae)):
        d1=ae[i]['data']
        aeterm_values.append(d1['aeterm']) #collecting aeterms values for finding duplicate entries of aeterm
        
    #print(parser.parse(d1[d]['aestdat']))
    try:
        aestdat = parser.parse(d1[d]['aestdat'])
        cmstdat = parser.parse(d2[d]['cmstdat'])
        aeendat = parser.parse(d1[d]['aeendat'])
        cmendat = parser.parse(d1[d]['cmendat'])
    except:
        pass
    
    delta = aestdat.date() - aeendat.date()
    inbetween_dates=[]
    for i in range(delta.days + 1):
        day = aestdat + timedelta(days=i)
        inbetween_dates.append(day) #finding inbetween dates from aestart to aeendat
    if aestdat > cmstdat: #type1
        #Submitting discrepancy data to the API 
        payload={
            "email_address": "vijisulochana14@gmail.com",
            "formname": d1[d]['formname'],
            "formid": d1[d]['formid'],
            "formidx": d1[d]['formidx'],
            "type": "Patients and rows for which Medication are given prior to the Adverse Events.",
            "subjectid": "TYPE1"
            }
        type1_url = "https://pyhack-dot-pharmanlp-177020.uc.r.appspot.com/api/1"+"/"+study_id
        jsonPayload = json.dumps(payload)
        try:
            r = requests.post(type1_url,headers=headers,data=jsonPayload)
        except:
            sys.exit("Unable to get the values!")
    if aeendat < cmstdat: #type2
        #Submitting discrepancy data to the API 
        payload={
            "email_address": "vijisulochana14@gmail.com",
            "formname": d1[d]['formname'],
            "formid": d1[d]['formid'],
            "formidx": d1[d]['formidx'],
            "type": "Patients and rows for which days Medications are given and Adverse Event occur don't match.",
            "subjectid": "TYPE2"
            }
        type2_url = "https://pyhack-dot-pharmanlp-177020.uc.r.appspot.com/api/1"+"/"+study_id
        jsonPayload = json.dumps(payload)
        try:
            r = requests.post(type1_url,headers=headers,data=jsonPayload)
        except:
            sys.exit("Unable to get the values!")
    if len(aeterm_values) != len(set(aeterm_values)): #type3 duplicate entries of AE event
        #Submitting discrepancy data to the API 
        payload={
            "email_address": "vijisulochana14@gmail.com",
            "formname": d1[d]['formname'],
            "formid": d1[d]['formid'],
            "formidx": d1[d]['formidx'],
            "type": "Duplicate Adverse events are entered or Adverse Events overlap.",
            "subjectid": "TYPE3"
            }
        type3_url = "https://pyhack-dot-pharmanlp-177020.uc.r.appspot.com/api/1"+"/"+study_id
        jsonPayload = json.dumps(payload)
        try:
            r = requests.post(type1_url,headers=headers,data=jsonPayload)
        except:
            sys.exit("Unable to get the values!")
    if cmendat == cmstdat: #type4
        #Submitting discrepancy data to the API 
        payload={
            "email_address": "vijisulochana14@gmail.com",
            "formname": d1[d]['formname'],
            "formid": d1[d]['formid'],
            "formidx": d1[d]['formidx'],
            "type": "Patients and rows which have overlapping of Concomitant medications.",
            "subjectid": "TYPE4"
            }
        type4_url = "https://pyhack-dot-pharmanlp-177020.uc.r.appspot.com/api/1"+"/"+study_id
        jsonPayload = json.dumps(payload)
        try:
            r = requests.post(type1_url,headers=headers,data=jsonPayload)
        except:
            sys.exit("Unable to get the values!")

    if cmstdat in inbetween_dates or cmstdat > aeendat: #type5 CM starts before the AE ends
        #Submitting discrepancy data to the API 
        payload={
            "email_address": "vijisulochana14@gmail.com",
            "formname": d1[d]['formname'],
            "formid": d1[d]['formid'],
            "formidx": d1[d]['formidx'],
            "type": "Patients for which the duration of Adverse Events is not adding up to corresponding concomitant medication.",
            "subjectid": "TYPE5"
            }
        type5_url = "https://pyhack-dot-pharmanlp-177020.uc.r.appspot.com/api/1"+"/"+study_id
        jsonPayload = json.dumps(payload)
        try:
            r = requests.post(type1_url,headers=headers,data=jsonPayload)
        except:
            sys.exit("Unable to get the values!")
    


