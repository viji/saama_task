**-------Modules Installation---------**
- $pip3 install -r requirements.txt

**-------Execution---------------**
- $python3 get_value.py

**-------Outputs-----------------**
- 1.Getting AE and CM subject id's(using API, /api/1/{study_id}/{domain_id}/subject/list)
- 2.Getting AE and CM data using subject id's (using API, /api/1/{study_id}/{domain_id}/subject/{subject_id}/list)
- 3.Process data and find discrepancy

  - 3.1 TYPE 1 - Patients and rows for which Medication are given prior to the Adverse Events.
  - 3.2 TYPE 2 - Patients and rows for which days Medications are given and Adverse Event occur don't match.
  - 3.3 TYPE 3 - Duplicate Adverse events are entered or Adverse Events overlap.
  - 3.4 TYPE 4 - Patients and rows which have overlapping of Concomitant medications.
  - 3.5 TYPE 5 - Patients for which the duration of Adverse Events is not adding up to corresponding concomitant medication.

- 4.Submitting discrepancy data to the API (use email address to submit the discrepancies)
